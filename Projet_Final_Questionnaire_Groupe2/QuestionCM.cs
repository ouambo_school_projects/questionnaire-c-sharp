﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    /// <summary>
    /// Question Choix Multiple (Plusieurs reponses et une seule vraie)
    /// Elle hérite de la classe Question;
    /// Elle implémente l'interface IExecution
    /// </summary>
    internal class QuestionCM : Question, IExecution
    {
        //Trois propriétés personnelles

        //Liste des propositions de reponses
        public List<string> ListeReponses { get; set; } = new List<string>();

        //La reponse vraie (indice)
        public int ReponseVraie { get; set; }

        //La reponse de l'eleve (indice)
        public int ReponseEleve { get; set; }

        /// <summary>
        /// Constructeur initialisant toutes les proprietes
        /// </summary>
        /// <param name="numero"></param>
        /// <param name="titre"></param>
        /// <param name="note"></param>
        /// <param name="listeReponses"></param>
        /// <param name="reponseVraie"></param>
        public QuestionCM(int numero, string titre, int note, List<string> listeReponses, int reponseVraie) : 
            base(numero, titre, note)
        {
            ListeReponses= listeReponses;
            ReponseVraie= reponseVraie;
            ReponseEleve = -1;              //Un nombre negatif pour preciser qu'il n'y a pas encore de reponse
        }


        /// <summary>
        /// Definition de la methode Executer de l'interface IExecution
        /// </summary>
        public void Executer(int numero)
        {
            Console.Clear();
            AfficherEnteteQuestion();
            Console.WriteLine();

            int reponse = 0;

            Tabulation(10);
            Console.ForegroundColor = green;
            Console.Write("Question " + numero + " : ");
            Console.WriteLine(Titre + "\n");
            Console.ResetColor();

            int i = 0;
            while (i < ListeReponses.Count)
            {
                Tabulation(15);
                FormaterIndiceQustion(i + 1, ListeReponses.ElementAt(i));
                Console.WriteLine();
                i++;
            }

            ControlerDonnees(1, ListeReponses.Count, out reponse);
            ReponseEleve = reponse-1;
        }

        /// <summary>
        /// redefinition de la methode CalculerNote() de la classe mere
        /// </summary>
        public override double CalculerNote()
        {
            return (ReponseEleve == ReponseVraie) ? NoteAttribuee : 0;
        }

        public override void AfficherResultat(int numero)
        {
            Tabulation(20);
            Console.ForegroundColor = green;
            Console.Write("Question " + numero + " : ");
            Console.WriteLine(Titre + "\n");
            Console.ResetColor();

            int i = 0;
            while (i < ListeReponses.Count)
            {
                Tabulation(30);
                FormaterIndiceQustion(i + 1, ListeReponses.ElementAt(i));
                Console.WriteLine();
                i++;
            }

            Console.WriteLine();
            Tabulation(25);
            Console.Write("Bonne réponse = ");
            Console.ForegroundColor = green;
            Console.WriteLine(ListeReponses.ElementAt(ReponseVraie)+ "\n");
            Console.ResetColor();

            Tabulation(25);
            Console.WriteLine("Votre réponse  = " + ListeReponses.ElementAt(ReponseEleve));

            Console.WriteLine();
            Tabulation(25);
            Console.WriteLine("Note Attribuée = " + NoteAttribuee);

            Console.WriteLine();
            Tabulation(25);
            Console.WriteLine("Note obtenue = " + CalculerNote() + "\n");
        }


        //ToString
        public override string ToString()
        {
            string chaine = new string("Liste des réponses :\n ");
            int i = 0;
            while(i < ListeReponses.Count)
            {
                chaine += "[" + (i+1) + "]" + ListeReponses.ElementAt(i) + "\n";
                i++;
            }

            chaine += "Bonne réponse = " + ReponseVraie +
                    "Votre reponse = " + ReponseEleve + 
                    "Note obtenue = " + CalculerNote();
            return chaine;
        }

        /// <summary>
        /// Deux questions sont identiques si elles ont le meme numéro.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object? obj)
        {
            if (obj == null) return false;
            if (!(obj is QuestionCM)) return false;

            QuestionCM questionCM = (QuestionCM)obj;
            return Numero == questionCM.Numero;
        }
    }
}
