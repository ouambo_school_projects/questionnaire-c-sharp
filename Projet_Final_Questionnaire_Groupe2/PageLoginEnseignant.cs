﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    internal class PageLoginEnseignant
    {
        //Les couleurs
        public static ConsoleColor darkmagnata = ConsoleColor.DarkMagenta;
        public static ConsoleColor darkcyan = ConsoleColor.DarkCyan;
        public static ConsoleColor cyan = ConsoleColor.Cyan;
        public static ConsoleColor menu = ConsoleColor.Gray;
        public static ConsoleColor red = ConsoleColor.Red;
        public static ConsoleColor green = ConsoleColor.DarkGreen;
        public static void Main2(string[] args)
        {
            Enseignant enseignant = new Enseignant("Enseignant", "Enseignant", "passwword", "code");

            enseignant.AfficherEntete();
            enseignant.Tabulation(35);
            Console.WriteLine("[1] Ajouter une question\n");
            enseignant.Tabulation(35);
            Console.WriteLine("[2] Supprimer une question\n\n");
            enseignant.Tabulation(10);
            Console.Write("Que voulez-vous faire ? \n");

            int choix = 0;
            enseignant.ControlerDonnees(1, 2, out choix);

            if (choix == 1)
            {
                enseignant.AjouterQuestion();
            }
            else
            {
                Console.WriteLine("option " + choix);
            }
        }
    }
}
