﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    /// <summary>
    /// Nous avons besoin d'une méthode Executer() dans la classe Questionnaire et dans 
    /// les classes QuestionVF, QuestionVFM, QuestionCM (sous classes Question).
    /// 
    /// Dans les sous classes Question, cette methode permettra à une question de s'afficher à l'écran et de
    /// permettre à l'élève de répondre.
    /// Dans la classe Questionnaire, cette methode affichera progressivement les questions du Questionnaire.
    /// 
    /// Toutes ces classes ne pouvant pas etre liées par la notion d'héritage, et compte tenu du fait
    /// que l'affichage varie en fonction de la classe, nous avons jugé nécessaire 
    /// de creer une interface qu'elles pouront implementer chacune à son gout.
    /// </summary>
    public interface IExecution
    {
        /// <summary>
        /// Methode à définir par les classes Questionnaire, QuestionVF, QuestionVFM et QuestionCM 
        /// </summary>
        void Executer(int numero);
    }
}
