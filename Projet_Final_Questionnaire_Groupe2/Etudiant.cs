﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    public class Etudiant : Personne
    {
        //Chemin du fichier de question
        public static string cheminFichierQuestion = "Question.txt";

        //attributs
        public string Matricule { get; set; }

        //Constructeur
        public Etudiant(string nom, string prenom, string password, string matricule) : 
            base(nom, prenom, password)
        {
            Matricule = matricule;
        }


        //Redefinition de la methode Connecter
        public override void Connecter()
        {
            
        }

        public void DemarrerQuestionnaire()
        {
            int nombreDeQuestionsChoisies = 0;
            Tabulation(20);
            Console.WriteLine("Choisissez le nombre de questions sollicitees entre 5 et 20:\n");
            ControlerDonnees(5, 20, out nombreDeQuestionsChoisies);

            List<Question> listeQuestions = new List<Question>();

             string[] listeQuestion = File.ReadAllLines(cheminFichierQuestion);

             listeQuestions = CreerQuestions(listeQuestion);

            List<Question> listeQuestionsCourante = new List<Question>();
            List<int> listeIndiceDejeUtilisees = new List<int>();


            int i = 0;
            int indice = -1;
            while(i < nombreDeQuestionsChoisies)
            {
                Random rnd = new Random();
                indice = rnd.Next(0, 20);
                
                if(!listeIndiceDejeUtilisees.Contains(indice)) 
                {
                    listeQuestionsCourante.Add(listeQuestions.ElementAt(indice));
                    listeIndiceDejeUtilisees.Add(indice);
                    i++;
                }
            }


            Console.WriteLine();

            Questionnaire questionnaire = new Questionnaire(listeQuestionsCourante, 10, 10);
            questionnaire.Executer(1);
            questionnaire.AfficherResultat();

            double noteTotal = questionnaire.getNoteTotal();
            double noteObtenue = questionnaire.CalculerScoreOtenu();

            Tabulation(20);
            Console.WriteLine("Le Score total est : " + noteTotal + " Le score de succes est de 60%\n");
            Tabulation(20);
            Console.WriteLine("Votre score est : " + noteObtenue + " Ce qui equivaut a : " + ((noteObtenue/noteTotal))*100.0 + "%\n");

            if (noteObtenue >= noteTotal * 0.6) 
            {
                Console.ForegroundColor = green;
                Tabulation(20);
                Console.WriteLine("Bravo !!! Vous avez reussi au questionnaire. ");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = red;
                Tabulation(20);
                Console.WriteLine("Desole !!! Vous n'avez pas reussi au questionnaire. Veuillez reessayer ");
                Console.ResetColor();
            }
        }

        public List<Question> CreerQuestions(string[] lignes)
        {
            List<Question> listeQuestions = new List<Question>();

             int i = 1;

              while(i < lignes.Length)
              {
                  string[] tab1 = lignes[i].Split("||");

                if ((tab1[1].Trim()).Equals("QuestionCM"))
                {
                    string ch = tab1[3];

                    string[] tab2 = ch.Split('|');

                    List<string> listReponses = new List<string>();

                    foreach(string s in tab2) { listReponses.Add(s.Trim()); }

                    int numeroQuestion = Int32.Parse(tab1[0].Trim());
                    string titreQuestion = tab1[2].Trim();
                    int reponseVraie = Int32.Parse(tab1[4].Trim());
                    int nbPoint = Int32.Parse(tab1[5].Trim());
                    QuestionCM question = new QuestionCM(numeroQuestion, titreQuestion, nbPoint, listReponses, reponseVraie);
                    listeQuestions.Add(question);
                }
                else if ((tab1[1].Trim()).Equals("QuestionVFM"))
                {
                    string ch = tab1[3];
                    string ch2 = tab1[4];

                    string[] tab2 = ch.Split('|');
                    List<string> listReponses = new List<string>();
                    foreach (string s in tab2) { listReponses.Add(s.Trim()); }

                    string[] tab3 = ch2.Split('|');
                    List<int> listReponsesVraies = new List<int>();
                    foreach (string s in tab3) { listReponsesVraies.Add(Int32.Parse(s.Trim())); }

                    int numeroQuestion = Int32.Parse(tab1[0].Trim());
                    string titreQuestion = tab1[2].Trim();
                    int nbPoint = Int32.Parse(tab1[5].Trim());

                    QuestionVFM questionVFM = new QuestionVFM(numeroQuestion, titreQuestion, nbPoint, listReponses, listReponsesVraies);
                    listeQuestions.Add(questionVFM);
                }
                else
                {
                    int numeroQuestion = Int32.Parse(tab1[0].Trim());
                    string titreQuestion = tab1[2].Trim();

                    bool etat = false;

                    if (((tab1[4].Trim()).ToUpper()).Equals("VRAI"))
                        etat = true;

                    int nbPoint = Int32.Parse(tab1[5].Trim());

                    QuestionVF questionVF = new QuestionVF(numeroQuestion, titreQuestion, nbPoint, etat);
                    listeQuestions.Add(questionVF);
                }
                i++;

              }

            return listeQuestions;
        }
    }
}
