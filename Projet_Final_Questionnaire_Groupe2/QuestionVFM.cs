﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    /// <summary>
    /// Question Vrai ou Faux multiple (Plusieurs reponses vraies)
    /// Elle hérite de la classe Question;
    /// Elle implémente l'interface IExecution
    /// </summary>
    internal class QuestionVFM : Question, IExecution
    {
        //Deux propriétés personnelles

        //Liste des propositions de reponses
        public List<string> ListeReponses { get; set; } = new List<string>();

        //Liste des reponses vrais (indices)
        public List<int> ListeReponsesVraies { get; set; } = new List<int>();   

        //Liste des reponses de l'eleve (indices)
        public List<int> ReponsesEleve{ get; set; }

        /// <summary>
        /// Constructeur initialisant toutes les propriétés
        /// </summary>
        /// <param name="numero"></param>
        /// <param name="titre"></param>
        /// <param name="note"></param>
        /// <param name="listeReponses"></param>
        /// <param name="listeReponsesVraies"></param>
        public QuestionVFM(int numero, string titre, int note, List<string> listeReponses, List<int> listeReponsesVraies) : 
            base(numero, titre, note)
        {
            ListeReponses= listeReponses;
            ListeReponsesVraies= listeReponsesVraies;

            ReponsesEleve = new List<int> { -1, -1};     //Des nombres négatifs pour préciser qu'il n'y a pas encore de réponse
        }


        /// <summary>
        /// Definition de la methode Executer de l'interface IExecution
        /// </summary>
        public void Executer(int numero)
        {
            Console.Clear();
            AfficherEnteteQuestion();
            Console.WriteLine();

            int reponse = 0;

            Tabulation(20);
            Console.ForegroundColor = green;
            Console.Write("Question " + numero + " : ");
            Console.WriteLine(Titre + "\n");
            Console.ResetColor();

            int i = 0;
            while (i < ListeReponses.Count)
            {
                Tabulation(30);
                FormaterIndiceQustion(i + 1, ListeReponses.ElementAt(i));
                Console.WriteLine();
                i++;
            }

            Console.WriteLine();
            Tabulation(10);
            Console.WriteLine(ListeReponsesVraies.Count + " réponses sont vraies : ");

            i = 0;
            ReponsesEleve.Clear();

            while(i < ListeReponsesVraies.Count)
            {
                Console.WriteLine();
                Tabulation(10);
                Console.Write("reponse " + (i+1) + "  ");
                ControlerDonnees(1, ListeReponses.Count, out reponse);
                ReponsesEleve.Add(reponse-1);
                i++;
            }
        }


        /// <summary>
        /// redefinition de la methode CalculerNote() de la classe mère
        /// Principe : Puisque plusieurs réponses sont vraies, la note attribuée à la question
        /// est partagée équitablement entre les réponses trouvées 
        /// </summary>
        public override double CalculerNote()
        {
            int nombreReponseVraies = 0;
            foreach(var entier in ReponsesEleve)
            {
                if(ListeReponsesVraies.Contains(entier))
                    nombreReponseVraies++;
            }

            return ((double)nombreReponseVraies / ListeReponsesVraies.Count) * NoteAttribuee;
        }

        public override void AfficherResultat(int numero)
        {
            Tabulation(20);
            Console.ForegroundColor = green;
            Console.Write("Question " + numero + " : ");
            Console.WriteLine(Titre + "\n");
            Console.ResetColor();

            int i = 0;
            while (i < ListeReponses.Count)
            {
                Tabulation(30);
                FormaterIndiceQustion(i + 1, ListeReponses.ElementAt(i));
                Console.WriteLine();
                i++;
            }

            Console.WriteLine();
            Tabulation(25);
            Console.Write("Liste des reponses vraies :  ");
            i = 0;
            while (i < ListeReponsesVraies.Count)
            {
                Console.ForegroundColor = green;
                Console.Write(ListeReponses.ElementAt(ListeReponsesVraies.ElementAt(i)) + ", ");
                i++;
            }
            Console.ResetColor();

            Console.WriteLine("\n");
            Tabulation(25);
            Console.Write("Vos reponses : ");
            i = 0;
            while (i < ReponsesEleve.Count)
            {
                Console.Write(ListeReponses.ElementAt(ReponsesEleve.ElementAt(i)) + ", ");;
                i++;
            }

            Console.WriteLine("\n");
            Tabulation(25);
            Console.WriteLine("Note Attribuée = " + NoteAttribuee);

            Console.WriteLine();
            Tabulation(25);
            Console.WriteLine("Note obtenue = " + CalculerNote() + "\n");

        }

        //ToString
        public override string ToString()
        {
            string chaine = new string("Liste des réponses :\n ");
            int i = 0;
            while (i < ListeReponses.Count)
            {
                chaine += "[" + (i + 1) + "]" + ListeReponses.ElementAt(i) + "\n";
                i++;
            }

            chaine += "Listes des réponses vraies\n";
            i = 0;
            while(i < ListeReponsesVraies.Count)
            {
                chaine += "["+ ListeReponses.ElementAt(i) + "]\n";
                i++;
            }

            chaine += "Vos reponses \n";

            i= 0;
            while(i < ReponsesEleve.Count)
            {
                chaine += "[" + ReponsesEleve.ElementAt(i) + "]\n";
                i++;
            }

            chaine+= "Note obtenue : " + CalculerNote();

            return chaine;
        }

        /// <summary>
        /// Deux questions sont identiques si elles ont le même numéro.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns> Vrai et Faux </returns>
        public override bool Equals(object? obj)
        {
            if (obj == null) return false;
            if (!(obj is QuestionVFM)) return false;

            QuestionVFM questionVFM = (QuestionVFM)obj;
            return Numero == questionVFM.Numero;
        }
    }
}
