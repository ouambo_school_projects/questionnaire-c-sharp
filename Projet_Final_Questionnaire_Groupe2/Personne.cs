﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    public abstract class Personne
    {
        //Les attributs
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Password { get; set; }


        //Les couleurs
        public static ConsoleColor darkmagnata = ConsoleColor.DarkMagenta;
        public static ConsoleColor darkcyan = ConsoleColor.DarkCyan;
        public static ConsoleColor cyan = ConsoleColor.Cyan;
        public static ConsoleColor menu = ConsoleColor.Gray;
        public static ConsoleColor red = ConsoleColor.Red;
        public static ConsoleColor green = ConsoleColor.DarkGreen;


        //COntructeur
        public Personne(string nom, string prenom, string password)
        {
            Nom= nom;
            Prenom= prenom;
            Password= password;
        }

        //Cette méthode sera redéfinie dans les classes filles (Enseignant et Etudiant)
        //Car un enseignant et un étudiant ne se connectent pas de la même manière.
        public abstract void Connecter();

        public void AfficherEntete()
        {
            //Personnalisation du titre de la console
            Console.Title = "@ Groupe 2 Programmation orientée objet en C# | Collège la cité | Hiver 2023";

            string bienvenue = @" 
     ********************************************************************************************
    *         _ _ _ _ _                                                                          *              
    *       /  _ _ _ _  \      Programmation orientée objet en C# - @Collège La Cité Hiver 2023  *
    *       | |        \ \                    Groupe 2 | Projet final                            *
    *       | |_ _ _ _ / /                                                                       *
    *       |  _ _ _ _  |                     Veuillez Repondre aux                              * 
    *       | |        \ \                      questions posées.                                *
    *       | |         \ \                                                                      *
    *       |  \_ _ _ _ / /                                                                      *
    *        \_ _ _ _ ___/ ienvenue ................sur notre questionnaire.                     *
    *                                                                                            *
     ******************************************************************************************** ";

            Console.ForegroundColor = menu;
            Console.WriteLine(bienvenue);
            Console.ResetColor();
            Console.WriteLine();
        }

        public void Tabulation(int nbTab)
        {
            Console.Write(new string(' ', nbTab));
        }

        public void ControlerDonnees(int borneInf, int borneSup, out int reponse)
        {
            do
            {
                Console.WriteLine();
                Tabulation(10);
                Console.Write("Votre reponse : ");
                if (Int32.TryParse(Console.ReadLine(), out reponse))
                {
                    Tabulation(10);
                    Console.ForegroundColor = red;
                    Console.Write((reponse < borneInf || reponse > borneSup) ? "Saisissez un entier compris entre "
                        + borneInf + " et " + borneSup : "");
                    Console.ResetColor();
                }
                else
                {
                    Tabulation(10);
                    Console.ForegroundColor = red;
                    Console.WriteLine("Type de donnée non valide! Saisissez un entier compris entre " + borneInf + " et " + borneSup);
                    Console.ResetColor();
                }
            } while (reponse < borneInf || reponse > borneSup);
        }

    }
}

