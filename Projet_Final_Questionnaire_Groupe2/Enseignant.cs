﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    public class Enseignant : Personne
    {
        //Chemin du fichier de question
        public static string cheminFichierQuestion = "Question.txt";

        //attributs
        public string Code { get; set; }

        //Constructeur
        public Enseignant(string nom, string prenom, string password, string code) :
            base(nom, prenom, password)
        {
            Code = code;
        }

        //Les methodes
        public bool AjouterQuestion()
        {
            int typeQuestion = 0;
            string question = "";
            string choix = "";
            int choixInt;
            int nbPoint = 0;

            Console.Clear();
            AfficherEntete();

            Tabulation(20);
            Console.WriteLine("[1] Question Vrai ou Faux\n");
            Tabulation(20);
            Console.WriteLine("[2] Question Vrai ou Faux Multiple\n");
            Tabulation(20);
            Console.WriteLine("[3] Question Choix multiple\n");
            Tabulation(20);
            Console.Write("Quel type de question voulez-vous ajouter ? \n");

            ControlerDonnees(1, 3, out typeQuestion);

            Console.WriteLine( );

            Tabulation(20); 
            Console.Write("Entrez votre question  : ");
            question = Console.ReadLine();

            Tabulation(20);
            Console.WriteLine("Nombre de point ");
            ControlerDonnees(1, 5, out nbPoint);

            if (typeQuestion == 1)
            {
                Console.WriteLine( );
                Tabulation(20);
                Console.Write("Reponse : [1] Pour Vrai ou [2] Pour Faux  : \n");
                ControlerDonnees(1, 2, out choixInt);
                choix = "" + choixInt;

                string question2 = "|QuestionVF     || " + question + "||" + "Vrai|Faux || " + (choix == "1" ? "Vrai" : "Faux") + "|| " + nbPoint;

               return InsererLigneDansFichier(question2);
            }
            else if(typeQuestion == 2)
            {
                Console.WriteLine();
                Tabulation(20);
                Console.Write("Nombre de choix de reponses : \n");
                int nombreChoix = 0;
                ControlerDonnees(1, 4, out nombreChoix);

                int i = 1;
                string[] choixReponses = new string[nombreChoix];
                List<int> choixVraies = new List<int>();

                Console.WriteLine();
                while (i <= nombreChoix)
                {
                    Tabulation(20);
                    Console.Write("Reponse " + i + " : ");
                    choixReponses[i - 1] = Console.ReadLine();

                    Tabulation(20);
                    Console.Write("Est une reponse Vrai ?  [1] pour Oui et [2] pour Non" );
                    int choixV = 0;
                    ControlerDonnees(1, 2, out choixV);

                    if (choixV == 1)
                        choixVraies.Add(i-1);

                    Console.WriteLine();
                    i++;
                }

                string question2 = "QuestionVFM     ||    " + question + "    ||    ";

                foreach (var ch in choixReponses) {  question2 += ch + "|";  }

                question2 += "|    ";

                foreach(var co in choixVraies) { question2 += co + "|"; }

                question2 += "|     " + nbPoint;

                InsererLigneDansFichier(question2);

                return true;
            }
            else
            {
                Console.WriteLine();
                Tabulation(20);
                Console.Write("Nombre de choix de reponses : \n");
                int nombreChoix = 0;
                ControlerDonnees(1, 4, out nombreChoix);

                int i = 1;
                string[] choixReponses = new string[nombreChoix];
                int choixSelect = -1;
                int choixVrai = -1;

                while (i <= nombreChoix)
                {
                    Tabulation(20);
                    Console.Write("Reponse " + i + " : ");
                    choixReponses[i - 1] = Console.ReadLine();

                    if (choixVrai == -1) 
                    {
                        Tabulation(20);
                        Console.Write("Est la reponse Vraie ?  [1] pour Oui et [2] pour Non");
                        ControlerDonnees(1, 2, out choixSelect);
                        if (choixSelect == 1)
                            choixVrai = i - 1;
                    }

                    Console.WriteLine();
                    i++;
                }

                string question2 = "QuestionQCM     ||    " + question + "    ||    ";

                foreach (var ch in choixReponses) { question2 += ch + "|"; }

                question2 += "|    " + choixVrai + "     ||    " + nbPoint;

                InsererLigneDansFichier(question2);

                return true;
            }
           
        }

        public bool InsererLigneDansFichier(string question2)
        {
            string[] listeQuestion = File.ReadAllLines(cheminFichierQuestion);

            string newQuestion = "||   "  + (listeQuestion.Length) + "      ||     " + question2;

            //Tentatative de sauvegarde
            try
            {
                //Ouverture du fichier de question en mode modification sans ecraser l'ancien contenu
                StreamWriter stream = new StreamWriter(cheminFichierQuestion, true, Encoding.ASCII);

                //stream.WriteLine(question);
                stream.WriteLine(newQuestion);
                stream.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Si la sauvegarde echoue, on retourne false
                return false;
            }
        }

        //Methode afficher Question
        public string[] AfficherToutesQuestion()
        {
            string[] listeQuestion = File.ReadAllLines(cheminFichierQuestion);

            foreach (var question in listeQuestion)
            {
                Console.WriteLine(question);
            }

            return listeQuestion;
        }

        //Methode supprimer une ligne du fichier
        public void SupprimerLigneFichier(int ligne)
        {
            string[] listeQuestion = File.ReadAllLines(cheminFichierQuestion);
            List<string> listeQuestion2 = new List<string>();
            int i = 0;
            while (i < listeQuestion.Length)
            {
                if (i != ligne)
                    listeQuestion2.Add(listeQuestion[i]);
                i++;
            }

            //Suppression du fichier
            File.Delete(cheminFichierQuestion);

            //Creation et Ouverture du fichier de question en mode modification sans ecraser l'ancien contenu
            StreamWriter stream = new StreamWriter(cheminFichierQuestion, true, Encoding.ASCII);

            foreach (var lig in listeQuestion2)
            {
                stream.WriteLine(lig);
            }
            stream.Close();
        }

        public bool SupprimerLigneFichier(string questionASupprimer)
        {
            string[] listeQuestion = File.ReadAllLines(cheminFichierQuestion);

            //Recherche de la question a supprimer dans le tableau
            for (int i = 0; i < listeQuestion.Length; i++)
            {
                if (listeQuestion[i].Contains(questionASupprimer))
                {
                    //Suppression de la question
                    listeQuestion = listeQuestion.Where((val, idx) => idx != i).ToArray();
                    //Ecriture des nouvelles questions dans le fichier
                    File.WriteAllLines(cheminFichierQuestion, listeQuestion);
                    return true;
                }
            }
            return false;
        }


        //Methode modifier une ligne du fichier
        public void ModifierLigneFichier(string questionAModifier, int typeQuestion, string nouvelleQuestion, string nouvelleReponse, int nbPoint)
        {
            string[] listeQuestion = File.ReadAllLines(cheminFichierQuestion);
            //Recherche de la question a modifier dans le tableau
            for (int i = 0; i < listeQuestion.Length; i++)
            {
                if (listeQuestion[i].Contains(questionAModifier))
                {
                    //Construction de la nouvelle ligne
                    string nouvelleLigne = "";
                    if (typeQuestion == 1)
                    {
                        nouvelleLigne = "|QuestionVF     || " + nouvelleQuestion + "||" + "Vrai|Faux || " + nouvelleReponse + "|| " + nbPoint;
                    }
                    else if (typeQuestion == 2)
                    {
                        nouvelleLigne = "|QuestionVFMulti|| " + nouvelleQuestion + "||" + "Vrai|Faux || " + nouvelleReponse + "|| " + nbPoint;
                    }
                    else if (typeQuestion == 3)
                    {
                        //Split de la chaine de la question a modifier pour obtenir le nombre de choix possibles
                        string[] questionSplit = questionAModifier.Split(new string[] { "||" }, StringSplitOptions.None);
                        int nbChoix = Int32.Parse(questionSplit[2].Split('|')[1]);



                        //Construction de la nouvelle ligne
                        nouvelleLigne = "|QuestionCM     || " + nouvelleQuestion + "||" + "NbChoix     || " + nbChoix + "||" + nouvelleReponse + "|| " + nbPoint;



                        //Ajout des choix possibles a la nouvelle ligne
                        for (int j = 0; j < nbChoix; j++)
                        {
                            Console.WriteLine("Entrez le choix possible numero " + (j + 1));
                            string choixPossible = Console.ReadLine();
                            nouvelleLigne += "||" + choixPossible;
                        }


                    }
                }
            }
        }

        //Redefinition de la methode Connecter
        public override void Connecter()
        {
            
        }
    }
}
