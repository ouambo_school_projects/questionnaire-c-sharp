﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    /// <summary>
    /// Classe de gestion des questions (constituer de questions de differents types)
    /// </summary>
    internal class Questionnaire : IExecution
    {
        //Les proprietes
        public List<Question> ListeDeQuestions { get; set; }
        public int ScoreDePassage { get; set; }

        public int NombreDeQuestions { get; set; }

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="listeDeQuestions"></param>
        /// <param name="scoreDePassage"></param>
        /// <param name="nombreDeQuestions"></param>
        public Questionnaire(List<Question> listeDeQuestions, int scoreDePassage, int nombreDeQuestions)
        {
            ListeDeQuestions= listeDeQuestions;
            ScoreDePassage= scoreDePassage;
            NombreDeQuestions= nombreDeQuestions;
        }


        /// <summary>
        /// Definition de la methode Executer de l'interface IExecution
        /// </summary>
        public void Executer(int numero)
        {
            Question questionCourant;
            int i = 0;
            while(i < ListeDeQuestions.Count)
            {
                questionCourant = ListeDeQuestions.ElementAt(i);

                if (questionCourant is QuestionCM)
                    ((QuestionCM)questionCourant).Executer(i + 1);
                else
                    if (questionCourant is QuestionVF)
                    ((QuestionVF)questionCourant).Executer(i + 1);
                else
                    ((QuestionVFM)questionCourant).Executer(i + 1);
                i++;
            }
        }


        /// <summary>
        /// Calcul du score obtenu après exécution du questionnaire
        /// </summary>
        /// <returns>Le score obtenu qui est un double</returns>
        public double CalculerScoreOtenu()
        {
            double score = 0;
            foreach(var question in ListeDeQuestions)
                score += question.CalculerNote();
            return score;
        }

        public double getNoteTotal()
        {
            double noteTotal = 0;
            foreach (var question in ListeDeQuestions)
                noteTotal += question.NoteAttribuee;
            return noteTotal;
        }


        /// <summary>
        /// Methode permettant d'afficher le résultat du auestionnaire après son exécution
        /// </summary>
        public void AfficherResultat()
        {
            Console.Clear();

            Question question1 = new Question(0, "0", 0);
            question1.AfficherEnteteQuestion();

            Console.WriteLine();
            Tabulation(45);
            Console.ForegroundColor = Question.darkcyan;
            Console.WriteLine("RESULTATS\n\n");
            Console.ResetColor();

            int i = 1;
            foreach(var question in ListeDeQuestions)
            {
                question.AfficherResultat(i);
                Console.WriteLine();
                i++;
            }
        }


        //ToString
        public override string ToString()
        {
            return base.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object? obj)
        {
            return base.Equals(obj);
        }

        public static void Tabulation(int nbTab)
        {
            Console.Write(new string(' ', nbTab));
        }
    }
}
