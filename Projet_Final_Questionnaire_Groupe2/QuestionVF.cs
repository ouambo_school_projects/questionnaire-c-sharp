﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Questionnaire
{
    /// <summary>
    /// Question Vrai ou Faux;
    /// Elle hérite de la classe Question;
    /// Elle implémente l'interface IExecution
    /// </summary>
    internal class QuestionVF : Question, IExecution
    {
        //Deux propriétés personnelles
        public bool Etat { get; set; }

        public bool ReponseEleve { get; set; }

        /// <summary>
        /// Constructeur faisant appel à la classe mère
        /// </summary>
        /// <param name="numero"></param>
        /// <param name="titre"></param>
        /// <param name="note"></param>
        /// <param name="etat"></param>
        public QuestionVF(int numero, string titre, int note, bool etat) : base(numero, titre, note)
        {
            Etat= etat;
            ReponseEleve = false;  //Par defaut on suppose que c'est faux
        }

        /// <summary>
        /// Definition de la methode Executer de l'interface IExecution
        /// </summary>
        public void Executer(int numero)
        {
            Console.Clear();
            AfficherEnteteQuestion();
            Console.WriteLine("\n");

            int reponse = 0;

            Tabulation(10);
            Console.ForegroundColor = green;
            Console.Write("Question " + numero + " : ");
            Console.WriteLine(Titre);
            Console.ResetColor();

            Console.WriteLine();
            Tabulation(15);
            FormaterIndiceQustion(1, "Vrai   ");
            FormaterIndiceQustion(2, "Faux\n");
            Console.WriteLine();

            Console.WriteLine();
            ControlerDonnees(1,2, out reponse);
            ReponseEleve = (reponse == 1) ? true : false;
        }


        /// <summary>
        /// redefinition de la methode CalculerNote() de la classe mere
        /// </summary>
        public override double CalculerNote(){ return (ReponseEleve == Etat) ? NoteAttribuee : 0; }

        public override void AfficherResultat(int numero)
        {
            Tabulation(20);
            Console.ForegroundColor = green;
            Console.Write("Question " + numero + " : ");
            Console.WriteLine(Titre);
            Console.ResetColor();

            Console.WriteLine();
            Tabulation(30);
            FormaterIndiceQustion(1, "Vrai   ");
            FormaterIndiceQustion(2, "Faux\n");
            Console.WriteLine();

            Tabulation(25);
            Console.Write("Bonne réponse = ");
            Console.ForegroundColor = green;
            Console.WriteLine(((Etat == true) ? "Vrai" : "Faux") + "\n");
            Console.ResetColor();

            Tabulation(25);
            Console.WriteLine("Votre réponse  = " + ((ReponseEleve == true) ? "Vrai" : "Faux"));

            Console.WriteLine();
            Tabulation(25);
            Console.WriteLine("Note Attribuée = " + NoteAttribuee);

            Console.WriteLine();
            Tabulation(25);
            Console.WriteLine("Note obtenue = " + CalculerNote() + "\n");
        }

        //ToString
        public override string ToString()
        {
            return base.ToString() +
                    "\n Bonne réponse = " + Etat + "\n" +
                    "Votre réponse = " + ReponseEleve +
                    "\nNote obtenue = " + CalculerNote();    
        }

        /// <summary>
        /// Deux questions sont identiques si elles ont le meme numéro.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns> Vrai et Faux </returns>
        public override bool Equals(object? obj)
        {
            if (obj == null) return false;
            if (!(obj is QuestionVF)) return false;

            QuestionVF questionVF = (QuestionVF)obj;
            return Numero == questionVF.Numero;
        }
    }
}
