﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questionnaire
{
    /// <summary>
    /// Classe mere dont les autres types de question hériteront.
    /// </summary>
    public class Question
    {
        //Quatres propriétés
        public int Numero { get; set; }
        public string Titre { get; set; }
        public int NoteAttribuee { get; set; }

        //Les couleurs
        public static ConsoleColor darkmagnata = ConsoleColor.DarkMagenta;
        public static ConsoleColor darkcyan = ConsoleColor.DarkCyan;
        public static ConsoleColor cyan = ConsoleColor.Cyan;
        public static ConsoleColor menu = ConsoleColor.Gray;
        public static ConsoleColor red = ConsoleColor.Red;
        public static ConsoleColor green = ConsoleColor.DarkGreen;

        //public double NoteObtenue { get; set; } = 0;   //La note obtenue est 0 lors de la création du questionnaire

        /// <summary>
        /// Constructeur qui initialise toutes les propriétés.
        /// </summary>
        /// <param name="numero"></param>
        /// <param name="titre"></param>
        /// <param name="note"></param>
        public Question(int numero, string titre, int noteAttribuee) 
        { 
            Numero = numero;
            Titre = titre;
            NoteAttribuee = noteAttribuee;
        }


        /// <summary>
        /// Methode qui oblige l'utilsateur à saisir un nonmbre entier compris entre deux entiers précis.
        /// Cette methode est identique pour toutes les classes filles. D'ou la nécessité de la définir dans 
        /// dans la classe mère.
        /// </summary>
        /// <param name="borneInf"></param>
        /// <param name="borneSup"></param>
        /// <param name="reponse"></param>
        public void ControlerDonnees(int borneInf, int borneSup, out int reponse)
        {
            do
            {
                Console.WriteLine();
                Tabulation(10);
                Console.Write("Votre reponse : ");
                if (Int32.TryParse(Console.ReadLine(), out reponse))
                {
                    Tabulation(10);
                    Console.ForegroundColor = red;
                    Console.Write((reponse < borneInf || reponse > borneSup) ? "Saisissez un entier compris entre " 
                        + borneInf + " et " + borneSup : "");
                    Console.ResetColor();
                }
                else
                {
                    Tabulation(10);
                    Console.ForegroundColor = red;
                    Console.WriteLine("Type de donnée non valide! Saisissez un entier compris entre " + borneInf + " et "+ borneSup);
                    Console.ResetColor();
                }
            } while (reponse < borneInf || reponse > borneSup);
        }

        /// <summary>
        /// Entete de toutes les questions lors de l'affichage
        /// </summary>
        public void AfficherEnteteQuestion()
        {
            //Personnalisation du titre de la console
            Console.Title = "@ Groupe 2 Programmation orientée objet en C# | Collège la cité | Hiver 2023";

            string bienvenue = @" 
     ********************************************************************************************
    *         _ _ _ _ _                                                                          *              
    *       /  _ _ _ _  \      Programmation orientée objet en C# - @Collège La Cité Hiver 2023  *
    *       | |        \ \                    Groupe 2 | Projet final                            *
    *       | |_ _ _ _ / /                                                                       *
    *       |  _ _ _ _  |                     Veuillez Repondre aux                              * 
    *       | |        \ \                      questions posées.                                *
    *       | |         \ \                                                                      *
    *       |  \_ _ _ _ / /                                                                      *
    *        \_ _ _ _ ___/ ienvenue ................sur notre questionnaire.                     *
    *                                                                                            *
     ******************************************************************************************** ";

            Console.ForegroundColor = menu;
            Console.WriteLine(bienvenue);
            Console.ResetColor();
        }


        /// <summary>
        /// Minie fonction permettant de deplacer le curseur à une position donnée 
        /// </summary>
        /// <param name="nbTab"></param>
        public static void Tabulation(int nbTab)
        {
            Console.Write(new string(' ', nbTab));
        }

        /// <summary>
        ///  Minie fonction permettant d'afficher les questions
        ///  ainsi que leurs indices, avec les couleurs.
        /// </summary>
        /// <param name="indice"></param>
        /// <param name="devise"></param>
        public static void FormaterIndiceQustion(int indice, string reponse)
        {
            Console.Write("[");
            Console.ForegroundColor = darkmagnata;
            Console.Write(indice);
            Console.ResetColor();
            Console.Write("] ");
            Console.ForegroundColor = darkcyan;
            Console.Write(reponse);
            Console.ResetColor();
        }

        public virtual void AfficherResultat(int numero)
        {

        }

        /// <summary>
        /// Méthode permettant de determiner la note de la question courante apres exécution 
        /// du questionnaire.  ==> Note obtenue = Note attribuée à la question si l'élève a trouvé
        /// la bonne réponse et 0 sinon.
        /// Chaque sous classe doit la redéfinir à son gout.
        /// </summary>
        public virtual double CalculerNote(){ return 0; }


        //Redifinition de ToString et Equals

        //ToString
        public override string ToString()
        {
            return "Note Attribuée = " + NoteAttribuee +
                    "\nIntitulé = " + Titre;
                    ;
        }
   
        /// <summary>
        /// 2 questions sont identiques si elles ont le meme numéro.
        /// En effet, dans notre base de données, chaque question sera identifée de
        /// façon unique par son numéro.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Vrai ou Faux</returns>
        public override bool Equals(object? obj)
        {
            if (obj == null) return false;
            if(!(obj is Question)) return false;

            Question question = (Question)obj;
            return Numero == question.Numero;
        }
    }
}
