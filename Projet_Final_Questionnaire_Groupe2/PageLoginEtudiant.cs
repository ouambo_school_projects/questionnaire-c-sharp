﻿namespace Questionnaire
{
    internal class PageLoginEtudiant
    {

        static void Main(string[] args)
        {
            string essai = ""; 

            do
            {
                Console.Clear();

                Etudiant etudiant = new Etudiant("Model", "Model", "Model", "Model");
                etudiant.AfficherEntete();
                etudiant.DemarrerQuestionnaire();

                Console.WriteLine("\n");
                etudiant.Tabulation(20);
                Console.Write("Voulez-vous reessayer ? O : Oui ou N : Non     ");

                essai = Console.ReadLine();

                if (!(essai.ToUpper() == "O"))
                {
                    Console.WriteLine("\n");
                    etudiant.Tabulation(20);
                    Console.Write("Merci et a la prochaine  \n");
                    
                    break;
                }
                   
            } while(true);
        }
    }
}